package com.example.resume_management_system;

import com.example.resume_management_system.service.EsServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

@SpringBootTest
class ResumeManagementSystemApplicationTests {
    @Autowired
    EsServiceImpl esService;

    @Test
    void contextLoads() throws IOException {
        esService.putData("hdfs://192.168.80.128:9000/1800300733石将煌自荐信.doc");
    }

}
