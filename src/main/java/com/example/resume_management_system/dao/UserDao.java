package com.example.resume_management_system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.resume_management_system.pojo.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface UserDao extends BaseMapper<User> {

    @Select("SELECT username,sex,tel,mail,password,account FROM user where account = #{account}")
    User getUserByAccount(String account);

    @Insert("INSERT INTO user(username,sex,tel,mail,password,account) VALUES(#{userName},#{sex},#{tel},#{mail},#{passWord},#{account})")
    int insertUser(User user);

    @Update("update user set password=#{newPassword} where account=#{account}")
    int updatePassword(String account,String newPassword);

}
