package com.example.resume_management_system.dao;

import com.example.resume_management_system.pojo.Start;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface StartDao {

    @Insert("INSERT INTO user_start(account,startFileUrl) VALUES(#{account},#{startFileUrl})")
    int insertUserStart(String account,String startFileUrl);

    @Select("SELECT account,startFileUrl FROM user_start WHERE account = #{account} and startFileUrl = #{startFileUrl}")
    Start selectStart(String account,String startFileUrl);

    @Select("SELECT account,startFileUrl FROM user_start WHERE account = #{account}")
    List<Start> selectStartList(String accountId);

    @Delete("DELETE FROM user_start WHERE account = #{account} and startFileUrl = #{startFileUrl}")
    int deleteUserStart(String account,String startFileUrl);

}
