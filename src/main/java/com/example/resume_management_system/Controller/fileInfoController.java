package com.example.resume_management_system.Controller;

import com.example.resume_management_system.Variable.Variable;
import com.example.resume_management_system.common.Result;
import com.example.resume_management_system.service.FileInfoServicelmpl;
import com.example.resume_management_system.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
public class fileInfoController {

    @Autowired
    ResultUtil resultUtil;

    @Autowired
    FileInfoServicelmpl fileInfoServicelmpl;


//    String url="hdfs://192.168.199.100:9000/";
    @RequestMapping("/fileinfo")
    public Result fileinfo(HttpServletRequest request){
        HttpSession session = request.getSession();
        String fileurl=request.getParameter("fileurl");

       // System.out.println(session.getAttribute("account").toString());
//        String account1 = "1";
        System.out.println();
        String account1 = (String)session.getAttribute("account");
//        if(account1 == null){
//            return  resultUtil.error(401,"尚未登录");
//        }
//        if(!account1.equals(account1)){
//            return resultUtil.error(400,"参数有误");
//        }
//        return resultUtil.ok(200,"ok",fileInfoServicelmpl.fileinfo(account1, Variable.url));
        if(Variable.account==null){
            return  resultUtil.error(401,"尚未登录");
        }
        if (fileurl==null){
            return resultUtil.ok(200,"ok",fileInfoServicelmpl.fileinfo(Variable.account, Variable.url));
        }
        return resultUtil.ok(200,"ok",fileInfoServicelmpl.fileinfo(Variable.account, fileurl));
    }
}
