package com.example.resume_management_system.Controller;

import com.example.resume_management_system.service.DeleteServicelmpl;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class DeleteController {
    @RequestMapping("delete")
    public String Delete(HttpServletRequest request){
        String deletepath=request.getParameter("deletepath");
        return new DeleteServicelmpl().DeleteServicelmpl(deletepath);
    }
}
