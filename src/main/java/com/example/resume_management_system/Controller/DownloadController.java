package com.example.resume_management_system.Controller;

import com.example.resume_management_system.service.DownloadServiceImpl;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class DownloadController {
    @Autowired
    DownloadServiceImpl downloadService;
    @RequestMapping("/download")
    public ResponseEntity<byte[]> download(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //获得
        String strpath=request.getParameter("strpath");
        System.out.println(strpath);
        HttpHeaders headers = new HttpHeaders();
        //通知浏览器以下载的方式打开文件
        headers.setContentDispositionFormData("attachment", new Path(strpath).getName().toString());
        //定义以流的形式下载返回文件数据
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);

        //使用springmvc框架的ResponseEntity对象封装返回数据
        return new ResponseEntity<byte[]>(downloadService.getFileByteArray(strpath), headers, HttpStatus.OK);
    }
}
