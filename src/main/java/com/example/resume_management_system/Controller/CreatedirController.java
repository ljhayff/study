package com.example.resume_management_system.Controller;

import com.example.resume_management_system.service.CreatedirServicelmpl;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class CreatedirController {
    @RequestMapping("createdir")
    public boolean createdir(HttpServletRequest request){
        String dirpath=request.getParameter("dirpath");
        String dirname=request.getParameter("dirname");
        if(dirpath==null||dirname==null)
            return false;
        if(dirpath.endsWith("/"))
            return new CreatedirServicelmpl().createdir(dirpath+dirname);
        else
            return new CreatedirServicelmpl().createdir(dirpath+"/"+dirname);
    }
}
