package com.example.resume_management_system.Controller;

import com.example.resume_management_system.common.Result;
import com.example.resume_management_system.dto.LoginForm;
import com.example.resume_management_system.service.FindPasswordServiceImpl;
import com.example.resume_management_system.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
public class FindPasswordController {
    @Autowired
    FindPasswordServiceImpl findPasswordService;

    @Autowired
    ResultUtil resultUtil;

    @RequestMapping("/toFindPasswordPage")
    public String toFindPasswordPage(){
        return "findPassword";
    }

    @PostMapping("/handleFindPassword")
    @ResponseBody
    public Result handleFindPassword(@RequestBody LoginForm account, HttpServletRequest httpServletRequest){

//        String account=request.getParameter("account");
        System.out.println(account.getAccount());
        String findPasswordResult = findPasswordService.findPasswordByMail(account.getAccount());
        System.out.println(findPasswordResult);
        return resultUtil.ok(200,findPasswordResult, account);
    }
}
