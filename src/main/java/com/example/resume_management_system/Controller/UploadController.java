package com.example.resume_management_system.Controller;

import java.io.File;

import com.example.resume_management_system.service.UploadServicelmpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Controller
public class UploadController {
    UploadServicelmpl upfile=new UploadServicelmpl();
    @GetMapping("/uploadtest")
    public String upload() {
        return "upload";
    }

    @PostMapping("/upload")
    @ResponseBody
    public boolean upload(@RequestParam("file") MultipartFile file,HttpServletRequest request){
        System.out.println("进入file");
        if (upfile.upload(file)){
            System.out.println(file);
//            System.out.println();
//            model.addAllAttributes((Map) new HashMap<>().put("result","1"));
            String hdfspath=request.getParameter("hdfspath");
            hdfspath="hdfs://192.168.80.128:9000/";

            if(upfile.copyFile(hdfspath))
                return true;
            return false;
//            return true;
        }
        System.out.println("失败");
//        model.addAllAttributes((Map) new HashMap<>().put("result","-1"));
        return false;
    }
//    @RequestMapping("uploadhdfs")
//    @ResponseBody
//    public boolean uploadhdfs(HttpServletRequest request){
//        String hdfspath=request.getParameter("hdfspath");
//        if(upfile.copyFile(hdfspath))
//            return true;
//        return false;
//    }
}
