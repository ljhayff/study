package com.example.resume_management_system.Controller;

import com.example.resume_management_system.pojo.fileInfo;
import com.example.resume_management_system.service.EsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@RestController
public class EsSearchController {
    @Autowired
    EsServiceImpl esService;
    @PostMapping("/home")
    public List<fileInfo> getSearchData(HttpServletRequest request) throws IOException {
        String searchStr=request.getParameter("searchStr");
        System.out.print(searchStr);
        return esService.getFileInfoListByFilePathList(esService.getFilePathBySearchStr(searchStr));
    }
}
