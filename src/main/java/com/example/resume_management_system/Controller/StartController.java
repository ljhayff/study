package com.example.resume_management_system.Controller;

import com.example.resume_management_system.common.Result;
import com.example.resume_management_system.dto.StartForm;
import com.example.resume_management_system.service.StartService;
import com.example.resume_management_system.utils.ResultUtil;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.hadoop.yarn.webapp.hamlet.Hamlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class StartController {

    @Autowired
    ResultUtil resultUtil;

    @Autowired
    StartService startService;

    @PostMapping("/start")
    public Result StartFile(@RequestBody StartForm sf, HttpServletRequest request){
        System.out.println(sf);
        String startFileUrl = sf.getStartFileUrl();
//        String account = "1";

        String account =  sf.getAccount();
        HttpSession session = request.getSession();
        String account1 = (String)session.getAttribute("account");

        return resultUtil.ok(200,startService.startFile(account,startFileUrl));
    }

    @GetMapping("/starts")
    public Result StartFile(@RequestParam String account){

        if(account == null){
            return  resultUtil.error(401,"尚未登录");
        }
        return ResultUtil.ok(200,"ok",startService.startList(account));
    }

}
