package com.example.resume_management_system.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
    @RequestMapping("/home")
    public String goHome(){
        return "home";
    }
    @RequestMapping("/tologin")
    public String gologin(){
        //测试
        return "login";
    }
    @RequestMapping("/toregister")
    public String goregister(){
        return "register";
    }
    // 石
    @RequestMapping("/toFindPassword")
    public String goFindPassword(){
        return "findPassword";
    }
}
