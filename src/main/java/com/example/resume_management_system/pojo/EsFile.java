package com.example.resume_management_system.pojo;

import lombok.Data;


@Data
public class EsFile {
    String fileName;
    String fileContent;

    public EsFile(String fileName, String fileContent) {
        this.fileContent = fileContent;
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFileContent() {
        return fileContent;
    }
}
