package com.example.resume_management_system.pojo;

public class fileInfo {
    private String name;
    private long filesize;
    private String path;
    private boolean isStrat;
    private boolean isDirectory;
    //构造函数
    public fileInfo(String name, long filesize, String path, boolean isStrat, boolean isDirectory) {
        this.name = name;
        this.filesize = filesize;
        this.path = path;
        this.isStrat = isStrat;
        this.isDirectory = isDirectory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getFilesize() {
        return filesize;
    }

    public void setFilesize(long filesize) {
        this.filesize = filesize;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isStrat() {
        return isStrat;
    }

    public void setStrat(boolean strat) {
        isStrat = strat;
    }

    public boolean isDirectory() {
        return isDirectory;
    }

    public void setDirectory(boolean directory) {
        isDirectory = directory;
    }
}
