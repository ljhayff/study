package com.example.resume_management_system.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Start {

    String account;
    String startFileUrl;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getStartFileUrl() {
        return startFileUrl;
    }

    public void setStartFileUrl(String startFileUrl) {
        this.startFileUrl = startFileUrl;
    }

}
