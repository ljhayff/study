package com.example.resume_management_system.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class User {

    String userName;
    char sex;
    String tel;
    String mail;
    String passWord;
    String account;

    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", sex=" + sex +
                ", tel='" + tel + '\'' +
                ", mail='" + mail + '\'' +
                ", passWord='" + passWord + '\'' +
                ", account='" + account + '\'' +
                '}';
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }
}
