package com.example.resume_management_system.service;

import com.example.resume_management_system.Variable.Variable;
import com.example.resume_management_system.pojo.fileInfo;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Service
public class FileInfoServicelmpl {

    @Autowired
    StartService startService;

    public List fileinfo(String account ,String url){
        FileStatus[] list=null;
//
        System.out.println("fileinfo account"+account);
        List<fileInfo> str = new ArrayList<fileInfo>();
        try {

            Configuration conf=new Configuration();
            FileSystem fs =FileSystem.get(URI.create(url),conf);
            Path p =new Path(url);
            list=fs.listStatus(p);

            for(FileStatus item:list){
                //这里没有得到是否被收藏的Boolean值
                str.add(new fileInfo(item.getPath().getName().toString(),item.getLen()/1024,item.getPath().toString(), startService.isStart(account, item.getPath().toString()),item.isDirectory()));
//                str.add(new fileInfo(item.getPath().getName().toString(),item.getLen()/1024,item.getPath().toString(), false,item.isDirectory()));//数据库暂时无法查询

            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return str;
    }

    public List fileinfo(String url){
        FileStatus[] list=null;
        List<fileInfo> str = new ArrayList<fileInfo>();
        try {
            Configuration conf=new Configuration();
            FileSystem fs =FileSystem.get(URI.create(url),conf);
            Path p =new Path(url);
            list=fs.listStatus(p);
            for(FileStatus item:list){
                //这里没有得到是否被收藏的Boolean值
//                str.add(new fileInfo(item.getPath().getName().toString(),item.getLen(),item.getPath().toString(), false,item.isDirectory()));
                str.add(new fileInfo(item.getPath().getName().toString(),item.getLen()/1024,item.getPath().toString(), startService.isStart(Variable.account, item.getPath().toString()),item.isDirectory()));
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return str;
    }
}
