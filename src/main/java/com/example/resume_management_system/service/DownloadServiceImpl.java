package com.example.resume_management_system.service;

import com.example.resume_management_system.Variable.Variable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
@Service
public class DownloadServiceImpl {

    //获取文件输入流
    public byte[] getFileByteArray(String strpath) throws IOException
    {
        Configuration conf = new Configuration();
        conf.set("fs.default.name ", Variable.url+strpath);
        FileSystem fs = FileSystem.get(conf);
        InputStream inputStream= fs.open(new Path(strpath));
        ByteArrayOutputStream outputStream=new ByteArrayOutputStream();
        byte[] buff=new byte[1024*4];
        int n=0;
        while (-1!= (n=inputStream.read(buff))){
            outputStream.write(buff,0,n);
        }
        return outputStream.toByteArray();
    }
}
