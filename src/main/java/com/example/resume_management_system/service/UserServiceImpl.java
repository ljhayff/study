package com.example.resume_management_system.service;

import com.example.resume_management_system.dao.UserDao;
import com.example.resume_management_system.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserDao userDao;

    @Override
    public List<User> getUserList() {
        return userDao.selectList(null);
    }

    @Override
    public User getUserByUserName(String userName) {
        return userDao.selectById(userName);
    }

    public User getUserByAccount(String account){
        return userDao.getUserByAccount(account);
    }

    @Override
    public int register(User user) {
        User user1 = userDao.getUserByAccount(user.getAccount());
        if(user1 != null) {
            return -1;
        }
        return userDao.insertUser(user);
    }

    @Override
    public int updatePassword(String account, String newPassword) {
        return userDao.updatePassword(account,newPassword);
    }
}
