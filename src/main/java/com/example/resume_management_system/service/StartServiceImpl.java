package com.example.resume_management_system.service;

import com.example.resume_management_system.common.strEdit;
import com.example.resume_management_system.dao.StartDao;
import com.example.resume_management_system.pojo.Start;
import com.example.resume_management_system.pojo.fileInfo;
import org.apache.tomcat.jni.FileInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class StartServiceImpl implements StartService {

    @Autowired
    StartDao startDao;

    @Autowired
    FileInfoServicelmpl fis;

    @Autowired
    strEdit str;

    @Override
    public String startFile(String account, String startFileUrl) {
        Start start = startDao.selectStart(account,startFileUrl);
        if(start == null){
            startDao.insertUserStart(account, startFileUrl);
            return "收藏成功";
        }else{
            startDao.deleteUserStart(account, startFileUrl);
            return "取消收藏成功";
        }
    }

    @Override
    public boolean isStart(String account, String startFileUrl) {

        System.out.println("数据库1   "+account);
        Start start = startDao.selectStart(account,startFileUrl);
        System.out.println("数据库2");
        if(start == null)
            return false;
        else
            return true;
    }


    @Override
    public List<fileInfo> startList(String account) {
        List<Start> startList = startDao.selectStartList(account);
        List<fileInfo> res = new LinkedList<>();
        for(Start temp : startList){
            System.out.println(temp.getStartFileUrl());
            List<fileInfo> fileinfo = fis.fileinfo(str.strEdit(temp.getStartFileUrl()));
            fileInfo r = null;
            for(fileInfo t :fileinfo){
                if(t.getPath().equals(temp.getStartFileUrl()))
                    r = t;
            }
            res.add(r);
        }
        return res;
    }
}
