package com.example.resume_management_system.service;

import com.example.resume_management_system.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class FindPasswordServiceImpl {

    @Autowired
    JavaMailSender javaMailSender;
    @Autowired
    UserService userService;
    @Async
    public String findPasswordByMail(String account){
        User user = userService.getUserByAccount(account);
        if (user == null) {
            return "账号未注册";
        }
        String mail=userService.getUserByAccount(account).getMail();

            SimpleMailMessage mailMessage=new SimpleMailMessage();
            mailMessage.setSubject("simpleMailMessage");
            try {
                String newPassword=UUID.randomUUID().toString().substring(1, 7);
                userService.updatePassword(account,newPassword);
                mailMessage.setText("您的密码已重置为：" +newPassword+",请您及时修改密码,确保账户安全!");
                mailMessage.setTo(mail);
                mailMessage.setFrom("2332762864@qq.com");
                javaMailSender.send(mailMessage);
                return "密码重置成功，请查看邮箱！";
            }
            catch (Exception e){
                e.printStackTrace();
                System.out.print("密码修改失败!未知错误。");
                return "密码修改失败!未知错误。";
        }
    }
}
