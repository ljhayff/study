package com.example.resume_management_system.service;

import org.apache.commons.lang.NullArgumentException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.PathIsNotEmptyDirectoryException;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

@Service
public class DeleteServicelmpl {
    public String DeleteServicelmpl(String deletepath){
        Configuration conf = new Configuration();
        System.out.println("删除"+deletepath);
        try{
            FileSystem fs = FileSystem.get(URI.create(deletepath),conf,"root");
            fs.delete(new Path(deletepath),false);
            fs.close();
        } catch (IllegalArgumentException e){
            e.printStackTrace();
            return deletepath+"不是有效的DFS文件名";
        } catch (org.apache.hadoop.ipc.RemoteException e){
            e.printStackTrace();
            return "要删除的目录不为空";
        }catch (org.apache.hadoop.fs.PathIsNotEmptyDirectoryException e){
            e.printStackTrace();
            return "要删除的目录不为空";
        }catch (NullPointerException e){
            e.printStackTrace();
            return "参数为null";
        }catch(Exception e){
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "删除失败";
        }

        return "删除成功";
    }
}
