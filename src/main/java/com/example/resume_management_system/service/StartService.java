package com.example.resume_management_system.service;

import com.example.resume_management_system.pojo.fileInfo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StartService {

    String startFile(String account,String startFileUrl);

    boolean isStart(String account,String startFileUrl);

    List<fileInfo> startList(String account);

}
