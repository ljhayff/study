package com.example.resume_management_system.service;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.URI;

@Service
public class UploadServicelmpl {
    public String localPath=null;
    public boolean upload(MultipartFile file) {
//        if (file.isEmpty()) {
////            return "上传失败，请选择文件";
//            return false;
//        }
        String fileName = file.getOriginalFilename();
        String filePath = "F:\\Desktop\\第14组\\upload\\";
        localPath=filePath+fileName;
        System.out.println(localPath);
        File dest = new File(localPath);
        try {
            file.transferTo(dest);
//            return "上传成功";
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
//        return "上传失败！";
        return true;
    }
    public boolean copyFile(String hdfsPath){
        Configuration conf = new Configuration();
        try {
            FileSystem fs = FileSystem.get(URI.create(hdfsPath),conf,"root");
            fs.copyFromLocalFile(new Path(localPath), new Path(hdfsPath));
            System.out.println("copy from: " + localPath + " to " + hdfsPath);
            fs.close();
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
