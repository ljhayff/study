package com.example.resume_management_system.service;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.stereotype.Service;

import java.net.URI;

@Service
public class CreatedirServicelmpl {
    public boolean createdir(String dirpath){
        try {
            System.out.println(dirpath);
            Configuration conf = new Configuration();
            FileSystem fs = FileSystem.get(URI.create(dirpath), conf,"root");
            Path f=new Path(dirpath);
            if (!fs.exists(new Path(dirpath))) {
                //fs.create(f);
                fs.mkdirs(f);
            }else{
                System.out.println("该文件已经存在或路径格式有误");
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        return true;
    }
}