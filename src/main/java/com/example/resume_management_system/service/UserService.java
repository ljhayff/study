package com.example.resume_management_system.service;

import com.example.resume_management_system.dto.LoginForm;
import com.example.resume_management_system.pojo.User;
import org.apache.zookeeper.Login;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface UserService {
        List<User> getUserList();
        User getUserByUserName(String userName);
        User getUserByAccount(String account);
        int register(User user);
        int updatePassword(String account,String newPassword);
}
