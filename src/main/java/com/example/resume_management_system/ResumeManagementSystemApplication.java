package com.example.resume_management_system;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@MapperScan("com.example.resume_management_system.dao")
public class ResumeManagementSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(ResumeManagementSystemApplication.class, args);
    }

}
