package com.example.resume_management_system.utils;

import com.example.resume_management_system.common.Result;
import org.springframework.stereotype.Component;

@Component
public class ResultUtil {

    public static Result ok(Integer aCode, String aMsg, Object aData){
        Result result = new Result();
        result.setCode(aCode);
        result.setMsg(aMsg);
        result.setData(aData);
        return result;
    }

    public static Result ok(Integer aCode,String aMsg){
        Result result = new Result();
        result.setCode(aCode);
        result.setMsg(aMsg);
        return result;
    }

    public static Result error(Integer aCode,String aMsg){
        Result result = new Result();
        result.setCode(aCode);
        result.setMsg(aMsg);
        return result;
    }


}
