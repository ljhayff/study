function checkEmail(email) {

    var pattern = /^[A-Za-zd]+([-_.][A-Za-zd]+)*@([A-Za-zd]+[-.])+[A-Za-zd]{2,5}$/;// 验证邮箱格式

    return pattern.test(email);
}

function checkPassword(password) {

    var pattern = /^[\\!@#$%^&*_\d\w]{6,18}$/;// 验证密码格式

    return pattern.test(password);
}
$('#btn_part1').click(function() { // 第一步

    var email     = $.trim($('#email').val());     // 邮箱
    var code      = $.trim($('#email_code').val());// 邮箱验证码
    if (!email) {
        layer.tips('<p style="font-size:18px;">请输入邮箱地址！</p>', '#email',{ tips: [3, '#fd5004']});
        return false;
    }

    if (!checkEmail(email)) {
        layer.tips('<p style="font-size:18px;">邮箱格式不正确！</p>', '#email',{ tips: [3, '#fd5004']});
        return false;
    }
    if (!code) {
        layer.tips('<p style="font-size:18px;">请输入验证码！</p>', '#email_code',{ tips: [3, '#fd5004']});
        if(code == "返回的验证码 " && email ==" "){
            window.location.replace("新密码界面");
        }
        else {return false}
    }



    $('#btn_part2').click(function(){

        var email       = $('#parent_2_email').val();
        var password    = $.trim($('#password').val());
        var re_password = $.trim($('#confirm_password').val());

        if (!password) {
            layer.tips('<p style="font-size:18px;">请输入密码！</p>', '#password',{ tips: [3, '#fd5004']});
            return false;
        }

        if (!checkPassword(password)) {
            layer.tips('<p style="font-size:18px;">密码只能由6至20字母数字和下划线“_”组成！</p>', '#password',{ tips: [3, '#fd5004']});
            return false;
        }

        if (!re_password) {
            return layer.tips('<p style="font-size:18px;">请确认新密码</p>', '#confirm_password',{ tips: [3, '#fd5004']});
        }

        if (password != re_password) {
            return layer.tips('<p style="font-size:18px;">密码不一致</p>', '#confirm_password',{ tips: [3, '#fd5004']});
        }


        $.post(
            "{{ url('changepasswordzc') }}",
            {
                email: email,
                password: password,
                _token: "{{ csrf_token() }}"
            },
            function(data,textStatus) {

                if (data.code == 0) {
                    layer.tips('<p style="font-size:18px;">'+data.msg+'</p>', '#btn_part2',{ tips: [3, '#fd5004']});
                }
                if (data.code == 1) {
                    $('#btn_part2').closest('.formSub').hide().next().show().next().hide();
                    $('#btn_part2').closest('.cbPassword').find('.Password li').eq(2).addClass('active').siblings().removeClass('active')
                }
            },
            "json"
        );
    })

