<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keyword" content="">

    <title></title>



    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
      <style>
          .search_form{
              width:602px;
              height:42px;
          }

          /*左边输入框设置样式*/
          .input_text{
              width:400px;
              height: 40px;
              border:1px solid green;
              /*清除掉默认的padding*/
              padding:0px;

              /*提示字首行缩进*/
              text-indent: 10px;

              /*去掉蓝色高亮框*/
              outline: none;

              /*用浮动解决内联元素错位及小间距的问题*/
              float:left;
          }

          .input_sub{
              width:100px;
              height: 42px;
              background: green;

              /*去掉submit按钮默认边框*/
              border:0px;
              /*改成右浮动也是可以的*/
              float:left;
              color:white;/*搜索的字体颜色为白色*/
              cursor:pointer;/*鼠标变为小手*/
          }
      </style>
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="http://127.0.0.1:8082/home" class="logo"><b>软通人力资源大数据</b></a>
            <!--logo end-->

            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout" href="#" onclick="outlogin()">退出登录</a></li>
            	</ul>
            </div>
        </header>
      <!--header end-->

      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">


              	  <p class="centered"><a href="http://127.0.0.1:8082/home"><img src="assets/img/ui-sam.jpg" class="img-circle" width="60"></a></p>
              	  <h5 class="centered">--</h5>
              <!-- 到这里 -->
                  <li class="sub-menu">
                      <a class="active" href="http://127.0.0.1:8082/home"  >
                          <i class="fa fa-th"></i>
                          	首页
                     </a>
                  <li class="sub-menu">
                      <a href="javascript:;" onclick="checkcollect()">
                          <i class=" fa fa-bar-chart-o"></i>
                          <span>收藏</span>
                      </a>
                  </li>
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<br><h3>软通人力资源大数据</h3>
                  <input type="text" class="input_text" id="seachtext" placeholder="请输入搜索内容"><!-- -->
                  <input type="button" value="搜索" class="input_sub" id="seach1" onclick="eseach();">
            </br>

              <div class="row mt">
                  <div class="col-md-12">
                      <div class="content-panel">
                          <table  id="mainform"class="table table-striped table-advance table-hover">

                          <div class="top-menu">
            				<ul class="nav pull-right top-menu">
                 			   <li><a class="logout" href="#" onclick="history.go(-1)">返回上一级</a></li>
            					</ul>
           						 </div>
           					<div class="top-menu">
            				<ul class="nav pull-right top-menu">
                 			   <li><a class="logout" href=#" onclick="mkdir1()">创建文件夹</a></li>
            					</ul>
           						 </div>
                              <div class="top-menu">
                                  <ul class="nav pull-right top-menu">
                                      <li><a class="logout" href="#" onclick="load1();">返回主页面</a></li>
                                  </ul>
                              </div>

                              <div class="top-menu">
                                  <ul class="nav pull-right top-menu">
                                      <li>

                                          <button class="logout" name="upload"  id="upload" >上传</button>
                                      </li>
                                  </ul>
                              </div>
                              <div class="top-menu">
                                  <ul class="nav pull-right top-menu">
                                      <li>

                                          <input type="file" name="file" id="file"  >
                                      </li>
                                  </ul>
                              </div>
	                  	  	  <h4 > 当前路径：</h4> <h4 id="currentpath" value=""></h4>

	                  	  	  <hr>
                              <thead>
                              <tr>
                                  <th><i class="fa fa-file"></i> 文件名</th>
                                  <th class="hidden-phone"><i class="fa fa-question-circle"></i> 大小</th>
                                  <th><i class="fa fa-download"></i> 下载</th>
                                  <th><i class=" fa fa-trash-o"></i> 删除</th>
                                   <th><i class=" fa fa-star"></i> 收藏</th>

                              </tr>
                              </thead>
                              <tbody>

                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->

		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->

  </section>

    <!-- js placed at the end of the document so the pages load faster -->
  <script src="assets/js/test.js"></script>

    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>

    <!--script for this page-->

  <script>
      $(document).ready(function(){
          load1();
      });

      $(function () {
          $("#upload").click(function(){
              var formData = new FormData();//创建FormData对象，将所需的信息封装到内部，以键值对的方式
              formData.append('file', $('#file')[0].files[0]);//参数封装格式,可以是文件，亦可以是普通的字符串
              formData.append('hdfsfilepath',$('#currentpath').text());
              $.ajax({
                  url: "http://127.0.0.1:8082/upload",
                  type: "post",
                  data: formData,
                cache:false,
                  contentType:false,
                  processData: false,
                  success: function (data) {
                      load($('#currentpath').text());
                      alert("success");
                  },
                  error: function (e) {
                      alert("上传失败！"+e.msg);

                  }
              });
          })
      });

      //custom select box


  </script>

  </body>
</html>
