<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>登录界面</title>
    <meta charset="utf-8">
    <link href="assets/css/login1.css" rel='stylesheet' type='text/css' />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link href="">
    <script src="assets/js/js1.js"></script>


</head>
<body>
<!-----start-main---->
<div class="main">
    <div class="login-form">
        <h1>登录</h1>
        <div class="head">
            <img src="assets/img/images/user.png" alt=""/>
        </div>
        <form >
            <input id="t1" name="account" type="text" class="USERNAME" value="用户名" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'USERNAME';}" >
            <input id="t2" name="password" type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}">
            <div class="submit">
                <input type="button" value="登录"  onclick="login()" >
            </div>
            <div class="submit">
                <%----%>
                <p><a href="http://localhost:8082/toregister" id="c1" >注册</a></p>
            </div>
            <p><a href="http://localhost:8082/toFindPassword" id="c3" >找回密码</a></p>
        </form>
    </div>
    <!--//End-login-form-->
    <!-----start-copyright---->
    <div class="copy-right">
        <p></p>
    </div>
    <!-----//end-copyright---->
</div>
<!-----//end-main---->
</body>
</html>