<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link href="assets/css/findPassword.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="container">
    <!-- findPW module -->
    <div class="findPW-box">
        <div id="findPW-title" >
            找回密码
        </div>

        <div class="input">
            <input type="text" id="account" placeholder="输入您的账号">
        </div>

        <div id="findPW">
            <input type="button" value="确认" onclick="findPassword()"/>
        </div>

        <div id="login-2">
            <input type="button" value="去登录/注册" onclick="toLogin()"/>
        </div>

    </div>
</div>

<%--<script type="text/javascript" src="assets/js/findPassword.js"></script>--%>
<script>
    //找回密码
    function findPassword(){
            $.ajax({
                type:"post",
                url:"http://127.0.0.1:8082/handleFindPassword",
                dataType:"json",
                async:false,
                cache:false,
                data:JSON.stringify({
                    "account":$("#account").val(),
                    "password": ""
                }),
                contentType: "application/json;charset=UTF-8",//指定消息请求类型
                success: function (data) {
                    alert(data.msg)
                    window.location.replace("http://localhost:8082/toFindPassword");
                }
                // error: function () {
                //     alert("找回密码失败");
                // }
            })
    }
    function toLogin(){
        window.location.replace("http://localhost:8082/tologin");
    }
</script>
</body>
</html>